package com.mx.redis.demo.repository;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.mx.redis.demo.model.User;

import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private RedisTemplate<String, User> redisTemplate;

    private HashOperations hashOperations;


    public UserRepositoryImpl(RedisTemplate<String, User> redisTemplate) {
        this.redisTemplate = redisTemplate;

        hashOperations = redisTemplate.opsForHash();
    }


    public void save(User user) {
        hashOperations.put("USER", user.getId(), user);
    }

    public Map<String, User> findAll() {
        return hashOperations.entries("USER");
    }

    public User findById(String id) {
        return (User)hashOperations.get("USER", id);
    }

    public void update(User user) {
        save(user);
    }

    public void delete(String id) {

        hashOperations.delete("USER", id);
    }
}
